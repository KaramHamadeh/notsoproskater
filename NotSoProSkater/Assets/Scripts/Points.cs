using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Points : MonoBehaviour
{
    public Text scoreText;

    public static int score;

    public bool canTurn = false;
    // Use this for initialization

    void Start()
    {
        score = 0;
        UpdateScore();
    }
    
    public void UpdateScore()
    {
        scoreText.text = "Score: " + score;

    }

    private void Update()
    {
        if (Input.GetKeyDown("space"))
        {
            UpdateScore();
            canTurn = true;
        }
        if (canTurn && Input.GetKeyDown(KeyCode.A))
        {
            UpdateScore();
        }
            
        if (canTurn && Input.GetKeyDown(KeyCode.D))
        {
            UpdateScore();
        }

        if (Input.GetKeyUp("space"))
        {
            canTurn = false;
        }
        
    }

}
